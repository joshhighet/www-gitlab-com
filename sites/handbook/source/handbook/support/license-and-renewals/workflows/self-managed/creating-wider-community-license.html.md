---
layout: markdown_page
title: Creating a wider community license
description: "How to create a wider community license"
category: GitLab Self-Managed licenses
---

{:.no_toc}

----

## Overview

Our [Code Contributor Program](/handbook/marketing/community-relations/code-contributor-program/#contributing-to-the-gitlab-enterprise-edition-ee) allows community contributors to contribute to GitLab Enterprise Edition. After trialling for 30 days, an EE license for 90 days can be generated if requested by a Contributor Success team member using [the internal request form](https://gitlab-com.gitlab.io/support/internal-requests-form/). Subsequent requests can be processed as necessary.

## Actioning

An internal request will be received [similar to this example](https://gitlab.zendesk.com/agent/tickets/293087), which should contain the required information to generate a license. Follow the [procedure to create a new license](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/self-managed/creating_licenses.html#create-a-new-license), ensuring the following:

- Plan: `Ultimate`, unless specified otherwise.
- Trial: `Yes` (is ticked)
- Starts at: (The current date)
- Expires at: (90 days or 1 year from the current date)

## Responding

Once you've generated the license, send a public response on the internal request including the requestor, who will be CC'd, confirming that the license has been generated and sent directly to the contact on the email address provided.
