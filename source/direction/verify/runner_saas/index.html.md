---
layout: markdown_page
title: "Category Direction - Runner SaaS"
description: "This is the Product Direction Page for the Runner SaaS product category."
canonical_path: "/direction/verify/runner_saas/"
---

## Navigation & Settings

|                       |                               |
| -                     | -                             |
| Stage                 | [Verify](/direction/verify/)  |
| Maturity              | Lovable |
| Content Last Reviewed | `2023-03-10`                  |

### Introduction and how you can help

Thanks for visiting this direction page on the Runner SaaS category at GitLab. This page belongs to the [Runner SaaS Group](https://about.gitlab.com/handbook/product/categories/#runner-saas-group) within the Verify Stage and is maintained by [Gabriel Engel](mailto:gengel@gitlab.com).

### Strategy and Themes

Runner SaaS is the product development effort for Runners on GitLab SaaS. Today those product offerings include:

- [GitLab SaaS Runners on Linux - GA](https://docs.gitlab.com/ee/ci/runners/saas/linux_saas_runner.html)
- [GitLab SaaS Runners on Windows - BETA](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html)
- [GitLab SaaS Runners on macOS x86-64 - BETA](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html)

### 1 year plan

The primary FY2024 Runner themes focus on closing critical competitive gaps with other hosted solutions and also enable Ultimate Adoption for GitLab SaaS.

- [GPU-enabled Runners](https://gitlab.com/groups/gitlab-org/-/epics/8648) for ModelOps workloads. (completed)
- [macOS](https://gitlab.com/groups/gitlab-org/-/epics/8267) and [Windows](https://gitlab.com/gitlab-org/gitlab/-/issues/300476) Runners transition out of beta.
- [Add more GCP compute types for SaaS Runners on Linux](https://gitlab.com/groups/gitlab-org/-/epics/8714).

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

In the next three months (March to May 2023) we plan to complete the following projects:

- Transition our current Limited Availability macOS Runners customers from existing x86-64 to Apple Silicon (M1) on AWS.

- Configure new GitLab.com Shared Runner Managers to enable the release of more GCP compute types for SaaS Runners on Linux.

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

- The top priority in the 15.11 milestone is to bring Apple Silicon (M1) SaaS macOS Runners to AWS. We will begin transitioning our Limited Availability customers after we fully move to AWS.

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

- We have implemented all background work to release [GPU-enabled Runners](https://gitlab.com/groups/gitlab-org/-/epics/8648) for ModelOps workloads in 15.10.

- In the past three months the team has setup scalable macOS SaaS Runners for testing, based on our new Taskscaler mechanism which will allow us to provide Apple Silicon (M1) machines on AWS Limited Availability and go to General Availablity later this year.

#### What is Not Planned Right Now

We are not actively working on the following:

- [GitLab SaaS Runners on Windows - GA](https://gitlab.com/gitlab-org/gitlab/-/issues/300476)
- [GitLab Arm-based SaaS Runners on Linux](https://gitlab.com/gitlab-org/gitlab/-/issues/388320)
- [SaaS Runners for Self-Managed GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/385199)

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecated near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

#### Key Capabilities

Organizations that use Cloud-native CI/CD solutions, such as GitLab.com, Harness.io, CircleCI, and GitHub, can run their CI/CD pipelines and get to a first green build without setting up build servers, installing and configuring build agents, or runners.

In addition to eliminating CI build server maintenance costs, there are other critical considerations for organizations that can migrate 100% of their CI/CD processes to a cloud-native solution. These include security, reliability, performance, multiple build server and configuration options, and on-demand scale. A critical strategic goal is to deliver solutions on GitLab SaaS that meet the most stringent compliance requirements and provides methods for securely connecting the GitLab SaaS Runner platform with a customer infrastructure. This, we believe, is the approach that will unlock the most value long-term for GitLab customers.

CI build speed or time-to-result, and the related CI build infrastructure cost efficiency are now critical competitive vectors. [CircleCI](https://circleci.com/circleci-versus-github-actions/) and [Harness.io](https://www.harness.io/blog/announcing-speed-enhancements-and-hosted-builds-for-harness-ci) are promoting CI-build performance in their go-to-market strategy.

Other industry participants, such as GitHub, have also invested in providing more computing choices for their hosted offerings. The recent [announcement](https://github.blog/2022-09-01-github-actions-introducing-the-new-larger-github-hosted-runners-beta/) by GitHub that new larger GitHub-hosted runners are now in beta, signals that the market is adopting CircleCI's position of providing more computing choices for customers that use SaaS-delivered CI/CD solutions. Finally, in recent blog posts, both [GitHub](https://github.blog/2022-12-08-experiment-the-hidden-costs-of-waiting-on-slow-build-times/) and [Harness.io](https://www.harness.io/blog/fastest-ci-tool) explored the cost impact of CI build performance measured by build times on hosted solutions.

#### Roadmap

In FY24 the key focus area for achieving best-in-class is [adding more GCP compute types for SaaS Runners on Linux](https://gitlab.com/groups/gitlab-org/-/epics/8714).

#### Top [1/2/3] Competitive Solutions

**Hosted Build Environment Resource Classes - non GPU enabled**

|Machine Class| Machine Type/Class | GitLab | GitHub | CircleCI |
| ------ | :-----: |  :-----: |  :-----:|:-----:|
|S| 1 vCPU, 4GB RAM | Available | Not available | Available |
|M| 2 vCPUs, 8GB RAM | Available | Available | Available |
|L| 4 vCPUs, 16GB RAM  | Available | Beta  | Available |
|XL | 8 vCPUs, 32GB RAM  | Not available | Beta| Available |
|2XL|16 vCPUs 64GB RAM| Not available | Beta| Available |
|3XL |32 vCPUs 128GB RAM|Not available|Beta|Avaiable on Windows|
|4XL|48 vCPUs 192GB RAM|Not available|Beta|Not available|
|5XL|64 vCPUs 256GB RAM|Not available|Beta|Not available|

**macOS - Offer Positioning and Hosted Build Machines**

|....||GitHub|Apple - Xcode Cloud GA |CircleCI|Bitrise.io|
|----------|----------------|----------------|----------------|----------------|----------------|
|Positioning Statement ||A GitHub-hosted runner is  VM hosted by GitHub with the GitHub actions runner application installed.|A CI/CD service built into Xcode, designed expressly for Apple developers.|Industry-leading speed. No other CI/CD platform takes performance as seriously as we do.|Build better mobile applications, faster|
|Value Proposition||When you use a GitHub-hosted runner, machine maintenance and upgrades are taken care of.|Build your apps in the cloud and eliminate dedicated build infrastructure.| The macOS execution environment allows you to test, build, and deploy macOS and iOS apps on CircleCI.|CI for mobile - save time spent on testing, onboarding, and maintenance with automated workflows and triggers|
|macOS Virtual Machine Specs||3-core CPU, 14 GB RAM |--|Medium: 4 vCPU, 8 GB RAM; Large: 8 vCPU, 16 GB RAM;  Metal 12 vCPU 32 GB RAM|Standard: 4 vCPU, 19 GB RAM; Elite 8 vCPU 35 GB ram; Elite XL 12 vCPU 54 GB RAM|

### Maturity Plan

- Runner SaaS is at the "Viable" maturity level - see our [definitions of maturity levels](/direction/maturity/).
