---
layout: markdown_page
title: "Category Direction - Subgroups"
description: "Groups are a fundamental building block (a small primitive) in GitLab for project organization and managing access to these resources at scale. Learn more!"
canonical_path: "/direction/enablement/subgroups/"
---

- TOC
{:toc}

| **Stage** | Group | **Maturity** | **Content Last Reviewed** |
| --- | --- | --- | -- |
| [Data Stores(/direction/enablement/) | [Organization](https://about.gitlab.com/handbook/product/categories/#organization-group) | [Complete](/direction/maturity/) | `2023-02-21` |

## Introduction and how you can help
Thanks for visiting this category direction page on Subgroups at GitLab. The Subgroups category is part of the [Organization Group](https://about.gitlab.com/handbook/product/categories/#organization-group) within the [Enablement](https://about.gitlab.com/direction/enablement/) section and is maintained by [Christina Lohr](https://about.gitlab.com/company/team/#lohrc). 

This vision and direction is constantly evolving and everyone can contribute: 
* Please comment in the linked issues and epics on this page. Sharing your feedback directly on GitLab.com or submitting a merge request to this page are the best ways to contribute to our strategy. 
* Please share feedback directly via email, or [schedule a video call](https://calendly.com/christinalohr/30min). If you're a GitLab user and have direct feedback about your needs for subgroups, we'd love to hear from you. 
* Please open an issue using the ~"Category:Subgroups" label, or reach out to us internally in the #g_manage_organization Slack channel.

## Overview

Groups are a fundamental building block (a [small primitive](https://about.gitlab.com/handbook/product/product-principles/#prefer-small-primitives)) in GitLab that serve to:
- Define organization boundaries, particularly on GitLab.com, where a top-level group frequently represents an organization
- Group users to manage project authorization at scale
- Create collections of users for security features like [code owners](https://docs.gitlab.com/ee/user/project/code_owners.html#code-owners) and [protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html#protected-environments). 
- Organize related projects together
- House features that cover multiple projects like epics, contribution analytics, and the security dashboard

### Our mission

In 2023, our goal is to make improvements by extending Subgroups to help enterprise organizations thrive on GitLab.com. We will accomplish that by iterating on existing constructs and streamlining workflows to guide users to intended usage patterns.

#### Challenges

Historically, enterprise customers have gravitated toward self-managed GitLab as their favored solution. With the proliferation of cloud services, enterprises are looking for options to use GitLab with managed infrastructure. We need to provide a SaaS platform that can safely and securely house our customers while reducing the load and cost of having to manage and configure their own infrastructure.

#### Opportunities

Iterating on Subgroups will allow us to solve a number of problems with the current SaaS experience:
* **Increase isolation** - We need mechanisms to isolate an organization from the rest of GitLab.com so that we can meet enterprise regulatory and security requirements. Creating clear organization boundaries in GitLab.com  will prevent users from unintentionally exposing users, projects, or other sensitive information to the rest of the GitLab.com instance. A top-level group should feel like a self-contained space where an enterprise can work independently of the rest of Gitlab.com. Users in a group owned by an enterprise should be able to complete all their day to day activities without leaving their organization's group.
* **Additional control** - On GitLab.com, user accounts and all associated details are owned by a user, not the groups they belong to. This is problematic for group administrators since they can't ensure the level of data integrity necessary for their audit and regulatory needs. We need to provide group administrators additional management capabilities for users in their group.
* **Flexible hierarchies** - We need to offer additional configuration options to allow enterprises to represent different [types of organizations](https://creately.com/blog/diagrams/types-of-organizational-charts/) using Subgroups.
* **Self-managed feature parity** - In order to satisfy the needs of enterprise customers on GitLab.com, we need to offer an equivalent to the group and project administration experience found in self-managed instances. To meet this goal, we must provide GitLab teams a framework to build settings and features and apply them to an entire group/project hierarchy.
 
## Target audience and experience

Groups are used by all GitLab users, no matter what role. However, heavier users of Groups in terms of organization and management of projects and users consist of:

* **Group Owners**
* **System Admins**
* **Team Leaders, Directors, Managers**
* **Individual contributors** who predominantly work with GitLab's project management features

## What's in progress, next and later

### In progress
* Building the foundation for the future Organization by [consolidating groups and projects](https://gitlab.com/groups/gitlab-org/-/epics/6473). As part of this effort we are:
  * [Migrating basic project functionality to namespaces](https://gitlab.com/groups/gitlab-org/-/epics/6585) as a baseline for making functionality available at the group level. This includes functionality like starring, archiving, transfer or deletion. 
  * Unifying members and member actions on the UI and API level by [consolidating memberships](https://gitlab.com/groups/gitlab-org/-/epics/8010).

### Next
As a result of the ongoing consolidation efforts, we will be able to make existing project functionality available at the group level. The first iterations of this will include:
* [Making archiving available at the group level](https://gitlab.com/gitlab-org/gitlab/-/issues/382051)
* [Allowing to star groups and subgroups](https://gitlab.com/groups/gitlab-org/-/epics/9298)

In addition, the new namespace framework also enables us to [move functionality from the admin level to the group level](https://gitlab.com/groups/gitlab-org/-/epics/7314):
* As a first iteration of this, we will focus on [migrating settings](https://gitlab.com/groups/gitlab-org/-/epics/4419), so that group/project owners can have more control over their members.

### Later
* We are planning further investments into [group sharing](https://gitlab.com/groups/gitlab-org/-/epics/8184) to align the behaviour between groups and projects. Part of this effort will be to [provide more visibility into group shares](https://gitlab.com/groups/gitlab-org/-/epics/9059) and to add an [invitation flow](https://gitlab.com/groups/gitlab-org/-/epics/9025) for group shares.


## What is not planned right now

* [Group managed accounts](https://docs.gitlab.com/ee/user/group/saml_sso/group_managed_accounts.html#group-managed-accounts) - While we previously implemented group managed accounts as a solution for increasing isolation in Gitlab.com, we're not planning to continue in this direction. We would like to de-couple functionality that was only available in group managed accounts to allow for greater flexibility.

## Maturity

This category is currently **Complete**. The next step in our maturity plan is achieving a **Lovable** state.
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.
* Note that a `Complete` state does not mean a category is "finished" and is no longer a priority. Even categories that are considered `Lovable` require continued investment.

## Top user issue(s)

Here is a list of the [top user upvoted issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=Category%3ASubgroups&label_name%5B%5D=group%3A%3Aorganization&first_page_size=100) with descending popularity:
* [Create an option to archive/unarchive Groups](https://gitlab.com/gitlab-org/gitlab/-/issues/15967)
* [Add support for subgroups in personal namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/16734)
* [README or "About" for groups](https://gitlab.com/gitlab-org/gitlab/-/issues/15041)

